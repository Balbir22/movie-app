package com.movieapp;

import android.app.Application;
import com.movieapp.dependencyinjection.AppComponent;
import com.movieapp.dependencyinjection.ContextModule;
import com.movieapp.dependencyinjection.DaggerAppComponent;
import com.movieapp.dependencyinjection.NetworkModule;
import com.movieapp.dependencyinjection.UtilsModule;


public class MovieApplication extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .utilsModule(new UtilsModule())
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
