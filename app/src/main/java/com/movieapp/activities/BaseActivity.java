package com.movieapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.google.gson.JsonSyntaxException;
import com.movieapp.MovieApplication;
import com.movieapp.utils.AppLoadingDialog;
import com.movieapp.utils.AppLogs;
import com.movieapp.utils.AppPrefs;
import com.movieapp.utils.AppToast;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity  {
    @Inject
    AppPrefs pAppPrefs;
    @Inject
    AppLogs pAppLogs;
    @Inject
    AppLoadingDialog pAppLoadingDialog;

    public MovieApplication getMovieApplication() {
        return ((MovieApplication) getApplication());
    }

    protected void showProgressDialog() {
        pAppLoadingDialog.show(this);
    }

    protected void hideProgressDialog() {
        pAppLoadingDialog.dismiss(this);
    }

    public void showWebServiceException(Context context, final Throwable t) {
        if (t != null) {
            if (t instanceof UnknownHostException) {
                AppToast.showToast(context, "UNKNOWN_HOST");
            } else if (t instanceof ConnectException) {
                AppToast.showToast(context, "UNKNOWN_HOST");
            } else if (t instanceof SocketTimeoutException) {
                AppToast.showToast(context, "SOCKET_TIME_OUT");
            } else if (t instanceof SocketException) {
                AppToast.showToast(context, "UNABLE_TO_CONNECT");
            } else if (t instanceof JsonSyntaxException) {
                AppToast.showToast(context, "JSON_SYNTAX");
            } else {
                if (!TextUtils.isEmpty(t.getMessage())) {
                    AppToast.showToast(context, t.getMessage());
                } else {
                    AppToast.showToast(context, "UNKNOWN_ERROR");
                }
            }
        }
    }


}