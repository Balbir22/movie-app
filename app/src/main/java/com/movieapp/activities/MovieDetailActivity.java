package com.movieapp.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.movieapp.R;
import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.model.MovieDetailModel;
import com.movieapp.utils.AppToast;
import com.movieapp.utils.AppUtils;
import com.movieapp.viewmodel.MovieDetailViewModel;
import com.movieapp.viewmodel.ViewModelFactory;

import javax.inject.Inject;


public class MovieDetailActivity extends BaseActivity {
    @Inject
    ViewModelFactory mViewModelFactory;
    @Inject
    Gson mGson;
    MovieDetailViewModel mMovieDetailViewModel;
    private int mMovieId;
    private ImageView posterImg;
    private TextView movieTitleTxt, runningTimeTxt, releaseDateTxt, generesNameTxt, ratingsNameTxt, actorsNameTxt, synopsysNameTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        setLayoutWidgetsId();
        getMovieApplication().getAppComponent().doMovieDetailActivityInjection(this);
        getIntentData();
        mMovieDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MovieDetailViewModel.class);
        getMovieApplication().getAppComponent().doMovieDetailViewModelInjection(mMovieDetailViewModel);
        performAPICall(mMovieId);
    }

    private void setLayoutWidgetsId() {
        posterImg = findViewById(R.id.posterImg);
        movieTitleTxt = findViewById(R.id.movieTitleTxt);
        runningTimeTxt = findViewById(R.id.runningTimeTxt);
        releaseDateTxt = findViewById(R.id.releaseDateTxt);
        generesNameTxt = findViewById(R.id.generesNameTxt);
        ratingsNameTxt = findViewById(R.id.ratingsNameTxt);
        actorsNameTxt = findViewById(R.id.actorsNameTxt);
        synopsysNameTxt = findViewById(R.id.synopsysNameTxt);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mMovieId = getIntent().getIntExtra("MOVIE_ID", 0);
        }
    }

    private void performAPICall(int movieId) {
        if (AppUtils.isNetworkEnabled(this)) {
            mMovieDetailViewModel.getDetailListMutableLiveData(movieId).observe(this, this::consumeResponse);
        } else {
            AppToast.showToast(this, "Network not found. Please change your network settings.");
        }
    }

    private void consumeResponse(RestApiResponse restApiResponse) {
        switch (restApiResponse.mStatus) {
            case ApplicationNetwork.Status.LOADING:
                showProgressDialog();
                break;
            case ApplicationNetwork.Status.SUCCESS:
                hideProgressDialog();
                setMovieDetailData(restApiResponse);
                break;
            case ApplicationNetwork.Status.ERROR:
                hideProgressDialog();
                showWebServiceException(this, restApiResponse.mThrowable);
                break;
            default:
                hideProgressDialog();
                break;
        }
    }

    private void setMovieDetailData(RestApiResponse restApiResponse) {
        Log.d("RESPONSE_SUCCESS", mMovieId + " : " + restApiResponse.mJsonElement.toString());
        MovieDetailModel movieDetailModel = mGson.fromJson(restApiResponse.mJsonElement, MovieDetailModel.class);
        if (movieDetailModel != null) {
            AppUtils.loadImageUsingGlide(this, posterImg, ApplicationNetwork.Urls.IMAGE_BASE_URL + (movieDetailModel.getPosterPath() == null ? "" : "" + movieDetailModel.getPosterPath()));
            //actorsNameTxt.setText(movieDetailModel.getTitle() == null ? "" : movieDetailModel.getTitle());
            runningTimeTxt.setText(movieDetailModel.getRuntime() == null ? "" : "" + movieDetailModel.getRuntime());
            releaseDateTxt.setText(movieDetailModel.getReleaseDate() == null ? "" : movieDetailModel.getReleaseDate());
            ratingsNameTxt.setText(movieDetailModel.getVoteAverage() == null ? "" : "" + movieDetailModel.getVoteAverage());
            synopsysNameTxt.setText(movieDetailModel.getOverview() == null ? "" : movieDetailModel.getOverview());
            movieTitleTxt.setText(movieDetailModel.getTitle() == null ? "" : movieDetailModel.getTitle());
            StringBuilder genreName = new StringBuilder();
            if (movieDetailModel.getGenres() != null) {
                int totalGenresSize = movieDetailModel.getGenres().size();
                int lastIndex = 1;
                for (int i = 0; i < totalGenresSize; i++) {
                    MovieDetailModel.Genre genre = movieDetailModel.getGenres().get(i);
                    if (genre != null && genre.getName() != null) {
                        if (lastIndex == totalGenresSize) {
                            genreName.append(genre.getName());
                        } else {
                            genreName.append(genre.getName()).append(" | ");
                        }
                    }
                    lastIndex++;
                }
            }
            generesNameTxt.setText(genreName);

        }
    }

  }
