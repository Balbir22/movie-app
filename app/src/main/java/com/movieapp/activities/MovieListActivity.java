package com.movieapp.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.movieapp.MovieApplication;
import com.movieapp.R;
import com.movieapp.adapters.MoviePagerAdapter;
import com.movieapp.utils.AppConstants;


public class MovieListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        ((MovieApplication) getApplication()).getAppComponent().doMovieListActivityInjection(this);
        setLayoutWidgetsId();
    }

    private void setLayoutWidgetsId() {
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.viewPager);
        setupMoviePagerAdapter(viewPager, tabLayout);
    }

    private void setupMoviePagerAdapter(ViewPager viewPager, TabLayout tabLayout) {
        MoviePagerAdapter pagerAdapter = new MoviePagerAdapter(getSupportFragmentManager(), AppConstants.ARR_MOVIE_TAB_NAME);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}
