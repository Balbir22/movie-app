package com.movieapp.utils;

public interface AppConstants {
    String NOW_PLAYING="Now Playing";
    String POPULAR="Popular";
    String[] ARR_MOVIE_TAB_NAME = {NOW_PLAYING, POPULAR};

    int VISIBLE_THRESHOLD = 2;
    int VIEW_TYPE_PROGRESS = 1;
    int VIEW_TYPE_ITEM = 2;
}
