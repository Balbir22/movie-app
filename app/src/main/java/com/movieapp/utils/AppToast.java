package com.movieapp.utils;

import android.content.Context;
import android.widget.Toast;

import com.movieapp.BuildConfig;

public final class AppToast {

    public static void showTestToast(Context context, String str) {
        if (BuildConfig.DEBUG) {
            if (context != null && str != null)
                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        }
    }

    public static void showToast(Context context, String str) {
        if (BuildConfig.DEBUG) {
            if (context != null && str != null)
                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        }
    }

}
