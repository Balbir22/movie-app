package com.movieapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;

import com.movieapp.R;

public class AppLoadingDialog {
    private Dialog dialog;

    public AppLoadingDialog() {
    }

    public void show(Activity activity) {
        if (!activity.isFinishing()) {
            if (dialog == null) {
                dialog = new Dialog(activity, android.R.style.Theme_Translucent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.include_loading_view);
            }
            dialog.show();
        }
    }

    public void dismiss(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog.cancel();
                }
            }
            dialog = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
