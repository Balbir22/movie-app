package com.movieapp.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.movieapp.R;
import com.movieapp.network.controller.ApplicationNetwork;

public final class AppUtils {

    /**
     * Check internet connection on device.
     * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> <br/>
     * <uses-permission android:name="android.permission.READ_PHONE_STATE" />
     *
     * @param pContext activities/fragment contexts
     * @return true/false
     */
    public static boolean isNetworkEnabled(Context pContext) {
        if (pContext != null) {
            try {
                ConnectivityManager conMngr = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMngr != null ? conMngr.getActiveNetworkInfo() : null;
                return activeNetwork != null && activeNetwork.isConnected();
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public static void loadImageUsingGlide(Context context, ImageView imageView,String url) {
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)
                .into(imageView);
    }

}
