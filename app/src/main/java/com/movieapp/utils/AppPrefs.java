package com.movieapp.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public final class AppPrefs {
    private final String PREF_NAME = "MovieAppPref";
    private SharedPreferences pref;
    private Editor editor;

    public AppPrefs(Context context) {
        setPref(context);
    }
    @SuppressLint("CommitPrefEdits")
    private void setPref(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    private void setCommitPref() {
        editor.commit();
    }

    public void setIntPref(String key, int value) {
        editor.putInt(key, value);
        setCommitPref();
    }

    public void setLongPref(String key, long value) {
        editor.putLong(key, value);
        setCommitPref();
    }

    public void setFloatPref(String key, float value) {
        editor.putFloat(key, value);
        setCommitPref();
    }

    public void setStringPref(String key, String value) {
        editor.putString(key, value);
        setCommitPref();
    }

    public void setBooleanPref(String key, boolean value) {
        editor.putBoolean(key, value);
        setCommitPref();
    }

    public long getLongPref(String key) {
        return pref.getLong(key, 0);
    }

    public int getIntPref(String key) {
        return pref.getInt(key, 0);
    }

    public float getFloatPref(String key) {
        return pref.getFloat(key, 0.0f);
    }

    public String getStringPref(String key) {
        return pref.getString(key, "");
    }

    public boolean getBooleanPref(String key) {
        return pref.getBoolean(key, false);
    }

    public void clear() {
        editor.clear();
        setCommitPref();
    }

}
