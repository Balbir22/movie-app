package com.movieapp.utils;

import android.util.Log;
import com.movieapp.BuildConfig;


public final class AppLogs {

    public void networkLog(String tag, String str) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, str);
        }
    }

    public void i(String TAG, String str) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, str);
        }
    }

    public void d(String TAG, String str) {
        if (BuildConfig.DEBUG) {
            i(TAG, str);
        }
    }

    public void e(String TAG, String str) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, str);
        }
    }

    public void exception(String TAG, Throwable t) {
        if (BuildConfig.DEBUG) {
            if (t != null) {
                e(TAG, t.getMessage() != null ? t.getMessage() : "Empty message");
                t.printStackTrace();
            }
        }
    }

}
