package com.movieapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.repository.Repository;

public class MovieListViewModel extends ViewModel {
    private MutableLiveData<RestApiResponse> mMovieList;
    private Repository mRepository;

    public MovieListViewModel(Repository mRepository) {
        this.mRepository = mRepository;
    }

    public MutableLiveData<RestApiResponse> getNowPlayingMovieListMutableLiveData(int page) {
        mMovieList = mRepository.getNowPlayingMovieList(ApplicationNetwork.UrlsParams.API_KEY, ApplicationNetwork.UrlsParams.LANGUAGE, page);
        return mMovieList;
    }

    public MutableLiveData<RestApiResponse> getPopularMovieListMutableLiveData(int page) {
        mMovieList = mRepository.getPopularMovieList(ApplicationNetwork.UrlsParams.API_KEY, ApplicationNetwork.UrlsParams.LANGUAGE, page);
        return mMovieList;
    }

}
