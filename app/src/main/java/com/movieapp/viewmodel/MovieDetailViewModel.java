package com.movieapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.repository.Repository;

public class MovieDetailViewModel extends ViewModel {
    private MutableLiveData<RestApiResponse> mMovieDetail;
    private Repository mRepository;

    public MovieDetailViewModel(Repository mRepository) {
        this.mRepository = mRepository;
    }

    public MutableLiveData<RestApiResponse> getDetailListMutableLiveData(int movieId) {
        mMovieDetail = mRepository.getDetailListMutableLiveData(movieId,ApplicationNetwork.UrlsParams.API_KEY,ApplicationNetwork.UrlsParams.LANGUAGE);
        return mMovieDetail;
    }

}
