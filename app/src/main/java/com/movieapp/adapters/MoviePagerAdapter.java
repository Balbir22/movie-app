package com.movieapp.adapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.movieapp.fragment.PopularMoviesFragment;
import com.movieapp.utils.AppConstants;

public class MoviePagerAdapter extends FragmentStatePagerAdapter {
    private String[] pageNameArr;

    public MoviePagerAdapter(FragmentManager fm, String[] pageNameArr) {
        super(fm);
        this.pageNameArr = pageNameArr;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(PopularMoviesFragment.ARG_TAB_NAME, AppConstants.ARR_MOVIE_TAB_NAME[position]);
        return PopularMoviesFragment.newInstance(bundle);
    }

    @Override
    public int getCount() {
        return pageNameArr == null ? 0 : pageNameArr.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return pageNameArr[position];
    }
}
