package com.movieapp.adapters.viewholder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.movieapp.R;
import com.movieapp.activities.MovieDetailActivity;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.model.MovieListModel;
import com.movieapp.utils.AppUtils;

import java.util.List;

public class MovieVH extends RecyclerView.ViewHolder {
    private ImageView posterImg;
    private TextView movieTitleTxt, ratingsNameTxt;

    public MovieVH(@NonNull View itemView) {
        super(itemView);
        posterImg = itemView.findViewById(R.id.posterImg);
        movieTitleTxt = itemView.findViewById(R.id.movieTitleTxt);
        ratingsNameTxt = itemView.findViewById(R.id.ratingsNameTxt);
    }

    public void setMovieVHolder(MovieVH movieVH, int position, List<MovieListModel.Result> movieList) {
        if (movieList != null && movieList.get(position) != null) {
            MovieListModel.Result result = movieList.get(position);
            AppUtils.loadImageUsingGlide(itemView.getContext(),movieVH.posterImg,ApplicationNetwork.Urls.IMAGE_BASE_URL + result.getPosterPath());
            movieTitleTxt.setText(getTitle(result));
            ratingsNameTxt.setText(getVoteAverage(result));
            movieVH.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), MovieDetailActivity.class);
                    intent.putExtra("MOVIE_ID",result.getId());
                    view.getContext().startActivity(intent);
                }
            });
        }
    }

    private String getVoteAverage(MovieListModel.Result result) {
        return result.getVoteAverage() != null ? "" + result.getVoteAverage() : "0.0";
    }

    private String getTitle(MovieListModel.Result result) {
        if (result.getTitle() == null) {
            return result.getOriginalTitle() != null ? result.getOriginalTitle() : "";
        } else {
            return result.getTitle();
        }
    }

}
