package com.movieapp.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.movieapp.R;


public class MoreProgressVH extends RecyclerView.ViewHolder{
    public ProgressBar progressBar;

    public MoreProgressVH(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
    }
}