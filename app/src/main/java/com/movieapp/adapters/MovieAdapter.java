package com.movieapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.movieapp.R;
import com.movieapp.adapters.viewholder.MoreProgressVH;
import com.movieapp.adapters.viewholder.MovieVH;
import com.movieapp.listeners.OnLoadMoreListener;
import com.movieapp.network.model.MovieListModel;
import com.movieapp.utils.AppConstants;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter {
    private final String TAG = "MovieAdapter-->>";
    private List<Integer> mViewTypeList;
    private List<MovieListModel.Result> mArrMovieListModelResult;
    private boolean isLoaded;
    private int mTotalItemCount, mLastVisibleItem;
    private OnLoadMoreListener mOnLoadMoreListener;

    public MovieAdapter(RecyclerView recyclerView, List<Integer> mViewTypeList, List<MovieListModel.Result> mArrMovieListModelResult) {
        this.mArrMovieListModelResult = mArrMovieListModelResult;
        this.mViewTypeList = mViewTypeList;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoaded && mTotalItemCount <= (mLastVisibleItem + AppConstants.VISIBLE_THRESHOLD)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case AppConstants.VIEW_TYPE_PROGRESS:
                viewHolder = new MoreProgressVH(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.more_progress_item, viewGroup, false));
                break;
            case AppConstants.VIEW_TYPE_ITEM:
                viewHolder = new MovieVH(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_vh_row, viewGroup, false));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case AppConstants.VIEW_TYPE_PROGRESS:
                ((MoreProgressVH) viewHolder).progressBar.setIndeterminate(true);
                break;
            case AppConstants.VIEW_TYPE_ITEM:
                MovieVH movieVH = (MovieVH) viewHolder;
                movieVH.setMovieVHolder(movieVH, position, mArrMovieListModelResult);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mViewTypeList ==null?0: mViewTypeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mViewTypeList.get(position);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded(boolean isLoaded) {
        this.isLoaded = isLoaded;
    }
}
