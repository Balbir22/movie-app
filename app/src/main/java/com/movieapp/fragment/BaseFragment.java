package com.movieapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonSyntaxException;
import com.movieapp.MovieApplication;
import com.movieapp.utils.AppLoadingDialog;
import com.movieapp.utils.AppLogs;
import com.movieapp.utils.AppPrefs;
import com.movieapp.utils.AppToast;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.inject.Inject;

public abstract class BaseFragment extends Fragment {
    protected AppCompatActivity pAppCompatActivity;
    @Inject
    AppPrefs pAppPrefs;
    @Inject
    AppLogs pAppLogs;
    @Inject
    AppLoadingDialog pAppLoadingDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pAppCompatActivity = (AppCompatActivity) context;
    }

    public MovieApplication getMovieApplication() {
        return ((MovieApplication) pAppCompatActivity.getApplication());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayoutId(), container, false);
    }

    protected abstract int getFragmentLayoutId();


    protected void showProgressDialog() {
        pAppLoadingDialog.show(pAppCompatActivity);
    }

    protected void hideProgressDialog() {
        pAppLoadingDialog.dismiss(pAppCompatActivity);
    }

    public void showWebServiceException(Context context, final Throwable t) {
        if (t != null) {
            if (t instanceof UnknownHostException) {
                AppToast.showToast(context, "UNKNOWN_HOST");
            } else if (t instanceof ConnectException) {
                AppToast.showToast(context, "UNKNOWN_HOST");
            } else if (t instanceof SocketTimeoutException) {
                AppToast.showToast(context, "SOCKET_TIME_OUT");
            } else if (t instanceof SocketException) {
                AppToast.showToast(context, "UNABLE_TO_CONNECT");
            } else if (t instanceof JsonSyntaxException) {
                AppToast.showToast(context, "JSON_SYNTAX");
            } else {
                if (!TextUtils.isEmpty(t.getMessage())) {
                    AppToast.showToast(context, t.getMessage());
                } else {
                    AppToast.showToast(context, "UNKNOWN_ERROR");
                }
            }
        }
    }

}
