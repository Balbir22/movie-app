package com.movieapp.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.google.gson.Gson;
import com.movieapp.R;
import com.movieapp.activities.MovieListActivity;
import com.movieapp.adapters.MovieAdapter;
import com.movieapp.listeners.OnLoadMoreListener;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.model.MovieListModel;
import com.movieapp.utils.AppConstants;
import com.movieapp.utils.AppToast;
import com.movieapp.utils.AppUtils;
import com.movieapp.utils.DividerItemDecoration;
import com.movieapp.viewmodel.MovieListViewModel;
import com.movieapp.viewmodel.ViewModelFactory;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class PopularMoviesFragment extends BaseFragment implements OnLoadMoreListener {
    public static String ARG_TAB_NAME = "tabName";
    private final String TAG = "PopularMoviesFragment-->>";
    @Inject
    ViewModelFactory mViewModelFactory;
    @Inject
    Gson mGson;
    MovieListViewModel mMovieListViewModel;
    private MovieListActivity mMovieListActivity;
    private RecyclerView popularMoviesRecycler;
    private MovieAdapter mMovieAdapter;
    private List<MovieListModel.Result> mArrMovieListModelResult;
    private List<Integer> mViewTypeList;
    private int mTotalPage, mCurrentPage = 1;
    private boolean mIsShowProgressbar = true;
    private String mTabName = "";

    public static PopularMoviesFragment newInstance(Bundle args) {
        PopularMoviesFragment fragment = new PopularMoviesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_popular_movies;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        onFetchBundleArg(getArguments());
        mMovieListActivity = (MovieListActivity) pAppCompatActivity;
        popularMoviesRecycler = view.findViewById(R.id.popularMoviesRecycler);
    }

    private void onFetchBundleArg(Bundle arguments) {
        if (arguments != null) {
            mTabName = arguments.getString(ARG_TAB_NAME, "");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getMovieApplication().getAppComponent().doPopularMoviesFragmentInjection(this);

        mViewTypeList = new ArrayList<>();
        mArrMovieListModelResult = new ArrayList<>();
        popularMoviesRecycler.addItemDecoration(new DividerItemDecoration(25, false));
        popularMoviesRecycler.setLayoutManager(new LinearLayoutManager(mMovieListActivity));
        mMovieAdapter = new MovieAdapter(popularMoviesRecycler, mViewTypeList, mArrMovieListModelResult);
        mMovieAdapter.setOnLoadMoreListener(this);
        popularMoviesRecycler.setAdapter(mMovieAdapter);
        mMovieListViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MovieListViewModel.class);
        getMovieApplication().getAppComponent().doMovieListViewModelInjection(mMovieListViewModel);
        performAPICall(mCurrentPage);
    }


    private void performAPICall(int mCurrentPage) {
        if (AppUtils.isNetworkEnabled(pAppCompatActivity)) {
            switch (mTabName) {
                case AppConstants.NOW_PLAYING:
                    mMovieListViewModel.getNowPlayingMovieListMutableLiveData(mCurrentPage).observe(this, this::consumeResponse);
                    break;
                case AppConstants.POPULAR:
                    mMovieListViewModel.getPopularMovieListMutableLiveData(mCurrentPage).observe(this, this::consumeResponse);
                    break;
                default:
                    break;
            }
        } else {
            AppToast.showToast(pAppCompatActivity, "Network not found. Please change your network settings.");
        }
    }

    private void consumeResponse(RestApiResponse restApiResponse) {
        switch (restApiResponse.mStatus) {
            case ApplicationNetwork.Status.LOADING:
                if (mCurrentPage == 1) {
                    showProgressDialog();
                }
                break;
            case ApplicationNetwork.Status.SUCCESS:
                hideProgressDialog();
                setAdapterListData(restApiResponse);
                break;
            case ApplicationNetwork.Status.ERROR:
                hideProgressDialog();
                showWebServiceException(pAppCompatActivity, restApiResponse.mThrowable);
                break;
            default:
                hideProgressDialog();
                break;
        }
    }

    private void setAdapterListData(RestApiResponse restApiResponse) {
        Log.d("RESPONSE_SUCCESS", mTabName + " : " + restApiResponse.mJsonElement.toString());
        MovieListModel movieListModel = mGson.fromJson(restApiResponse.mJsonElement, MovieListModel.class);
        if (movieListModel != null) {
            mTotalPage = movieListModel.getTotalPages();
            try {
                if (!mIsShowProgressbar) {
                    mViewTypeList.remove(mViewTypeList.size() - 1);
                    mMovieAdapter.notifyItemRemoved(mViewTypeList.size());
                    mMovieAdapter.setLoaded(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (MovieListModel.Result result: movieListModel.getResults()) {
                mViewTypeList.add(AppConstants.VIEW_TYPE_ITEM);
            }
            mArrMovieListModelResult.addAll(movieListModel.getResults());
            mMovieAdapter.notifyDataSetChanged();
            mMovieAdapter.setLoaded(false);
        }
    }

    @Override
    public void onLoadMore() {
        mMovieAdapter.setLoaded(true);
        if ((mTotalPage - 1) > mCurrentPage) {
            mCurrentPage++;
            mViewTypeList.add(AppConstants.VIEW_TYPE_PROGRESS);
            popularMoviesRecycler.post(() -> {
                mMovieAdapter.notifyItemInserted(mViewTypeList.size() - 1);
                mIsShowProgressbar = false;
                performAPICall(mCurrentPage);
            });
        }
    }
}
