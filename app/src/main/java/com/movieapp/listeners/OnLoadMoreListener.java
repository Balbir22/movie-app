package com.movieapp.listeners;

/**
 *
 * Listener called when page is scrolled down to threshold value
 */
public interface OnLoadMoreListener {
    /**
     * Calls api for pagination with next page
     */
    void onLoadMore();
}