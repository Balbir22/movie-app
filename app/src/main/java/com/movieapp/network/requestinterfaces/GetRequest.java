package com.movieapp.network.requestinterfaces;

import com.google.gson.JsonElement;
import com.movieapp.network.controller.ApplicationNetwork;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetRequest {
    @GET(ApplicationNetwork.Urls.NOW_PLAYING)
    Call<JsonElement> getNowPlayingMovieList(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);

    @GET(ApplicationNetwork.Urls.POPULAR)
    Call<JsonElement> getPopularMovieList(@Query("api_key") String api_key, @Query("language")String language, @Query("page") int page);

    @GET(ApplicationNetwork.Urls.MOVIE_DETAIL)
    Call<JsonElement> getDetailListMutableLiveData(@Path("movie_id") int movie_id, @Query("api_key") String api_key, @Query("language") String language);


}
