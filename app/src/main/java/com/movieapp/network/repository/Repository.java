package com.movieapp.network.repository;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.google.gson.JsonElement;
import com.movieapp.network.controller.NetworkBoundResource;
import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.requestinterfaces.GetRequest;

import retrofit2.Call;


public class Repository {
    private final GetRequest mGetRequest;

    public Repository(GetRequest mGetRequest) {
        this.mGetRequest = mGetRequest;
    }

    public MutableLiveData<RestApiResponse> getNowPlayingMovieList(String api_key, String language, int page) {
        return new NetworkBoundResource() {
            @NonNull
            @Override
            protected Call<JsonElement> createCall() {
                return mGetRequest.getNowPlayingMovieList(api_key, language, page);
            }
        }.getAsLiveData();
    }

    public MutableLiveData<RestApiResponse> getPopularMovieList(String api_key, String language, int page) {
        return new NetworkBoundResource() {
            @NonNull
            @Override
            protected Call<JsonElement> createCall() {
                return mGetRequest.getPopularMovieList(api_key, language, page);
            }
        }.getAsLiveData();
    }

    public MutableLiveData<RestApiResponse> getDetailListMutableLiveData(int movieId, String api_key, String language) {
        return new NetworkBoundResource() {
            @NonNull
            @Override
            protected Call<JsonElement> createCall() {
                return mGetRequest.getDetailListMutableLiveData(movieId,api_key, language);
            }
        }.getAsLiveData();
    }

}
