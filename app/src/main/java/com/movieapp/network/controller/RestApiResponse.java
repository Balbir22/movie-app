package com.movieapp.network.controller;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.JsonElement;


public class RestApiResponse {
    @Nullable
    public final JsonElement mJsonElement;
    @Nullable
    public final Throwable mThrowable;
    public String mStatus;

    private RestApiResponse(String mStatus, @Nullable JsonElement mJsonElement, @Nullable Throwable mThrowable) {
        this.mStatus = mStatus;
        this.mJsonElement = mJsonElement;
        this.mThrowable = mThrowable;
    }

    public static RestApiResponse loading() {
        return new RestApiResponse(ApplicationNetwork.Status.LOADING, null, null);
    }

    public static RestApiResponse success(@NonNull JsonElement data) {
        return new RestApiResponse(ApplicationNetwork.Status.SUCCESS, data, null);
    }

    public static RestApiResponse error(@NonNull Throwable error) {
        return new RestApiResponse(ApplicationNetwork.Status.ERROR, null, error);
    }

}
