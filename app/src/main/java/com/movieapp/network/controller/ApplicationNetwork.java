package com.movieapp.network.controller;

public interface ApplicationNetwork {

    interface Status {
        String SUCCESS = "SUCCESS";
        String ERROR = "ERROR";
        String LOADING = "LOADING";
    }

    interface TimeOut {
        long CONNECTION_TIMEOUT = 2000;
        long WRITE_TIMEOUT = 2000;
        long READ_TIMEOUT = 3000;
    }

    interface Urls {
        String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w138_and_h175_face/";
        String NOW_PLAYING="3/movie/now_playing";
        String POPULAR="3/movie/popular";
        String MOVIE_DETAIL = "3/movie/{movie_id}";
    }

    interface UrlsParams {
        String API_KEY = "398299e91a7c4901d6e67ff6e4cfc692";
        String LANGUAGE = "en-US";
    }
}
