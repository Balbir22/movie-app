package com.movieapp.network.controller;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.stream.MalformedJsonException;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;


public abstract class NetworkBoundResource {

    private final MutableLiveData<RestApiResponse> mApiResponseMutableLiveData = new MutableLiveData<>();

    @MainThread
    protected NetworkBoundResource() {
        mApiResponseMutableLiveData.setValue(RestApiResponse.loading());
        fetchFromNetwork();
    }

    private void fetchFromNetwork() {
        createCall().enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.body()!=null){
                    Log.d("RESPONSE_SUCCESS",response.body().toString());
                    mApiResponseMutableLiveData.setValue(RestApiResponse.success(response.body()));
                }else{
                   Log.d("RESPONSE_SUCCESS",response.toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                Log.e("RESPONSE_SUCCESS","ERROR",t);
                mApiResponseMutableLiveData.setValue(RestApiResponse.error(t));
            }
        });
    }

    @NonNull
    @MainThread
    protected abstract Call<JsonElement> createCall();

    public final MutableLiveData<RestApiResponse> getAsLiveData() {
        return mApiResponseMutableLiveData;
    }
}
