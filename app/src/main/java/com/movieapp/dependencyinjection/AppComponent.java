package com.movieapp.dependencyinjection;


import com.movieapp.activities.MovieDetailActivity;
import com.movieapp.activities.MovieListActivity;
import com.movieapp.fragment.PopularMoviesFragment;
import com.movieapp.viewmodel.MovieDetailViewModel;
import com.movieapp.viewmodel.MovieListViewModel;
import javax.inject.Singleton;
import dagger.Component;


@Component(modules = {ContextModule.class, UtilsModule.class, NetworkModule.class})
@Singleton
public interface AppComponent {
    void doMovieDetailActivityInjection(MovieDetailActivity movieDetailActivity);
    void doPopularMoviesFragmentInjection(PopularMoviesFragment popularMoviesFragment);
    void doMovieListActivityInjection(MovieListActivity loginActivity);
    void doMovieListViewModelInjection(MovieListViewModel loginViewModel);
    void doMovieDetailViewModelInjection(MovieDetailViewModel movieDetailViewModel);

}
