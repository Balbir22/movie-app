package com.movieapp.dependencyinjection;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movieapp.BuildConfig;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.requestinterfaces.GetRequest;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return builder.setLenient().create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    GetRequest getGetRequestInterface(Retrofit retrofit) {
        return retrofit.create(GetRequest.class);
    }

    @Provides
    @Singleton
    OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .build();
            return chain.proceed(request);
        })
                .connectTimeout(ApplicationNetwork.TimeOut.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(ApplicationNetwork.TimeOut.WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(ApplicationNetwork.TimeOut.READ_TIMEOUT, TimeUnit.SECONDS);

        return httpClient.build();
    }

}
