package com.movieapp.dependencyinjection;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;

import com.movieapp.network.controller.RestApiResponse;
import com.movieapp.network.requestinterfaces.GetRequest;
import com.movieapp.network.repository.Repository;
import com.movieapp.utils.AppLoadingDialog;
import com.movieapp.utils.AppLogs;
import com.movieapp.utils.AppPrefs;
import com.movieapp.viewmodel.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    @Singleton
    Repository getRepository(GetRequest apiCallInterface) {
        return new Repository(apiCallInterface);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory getViewModelFactory(Repository myRepository) {
        return new ViewModelFactory(myRepository);
    }

    @Provides
    @Singleton
    AppLogs getAppLogs() {
        return new AppLogs();
    }

    @Provides
    @Singleton
    AppPrefs getAppPrefs(Context context) {
        return new AppPrefs(context);
    }

    @Provides
    @Singleton
    AppLoadingDialog getAppLoadingDialog() {
        return new AppLoadingDialog();
    }

    @Provides
    MutableLiveData<RestApiResponse> getMutableLiveData() {
        return new MutableLiveData<RestApiResponse>();
    }
}
