package com.movieapp.api;


import com.movieapp.BuildConfig;
import com.movieapp.network.controller.ApplicationNetwork;
import com.movieapp.network.requestinterfaces.GetRequest;
import com.movieapp.utils.RequestInterceptor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ApiServiceUnitTest {

    private GetRequest apiService;
    @Before
    public void createService() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(ApplicationNetwork.TimeOut.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.readTimeout(ApplicationNetwork.TimeOut.READ_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.writeTimeout(ApplicationNetwork.TimeOut.WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.addInterceptor(new RequestInterceptor());
        okHttpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        apiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient.build())
                .build()
                .create(GetRequest.class);
    }


    @Test
    public void getPopularArticles() {
        try {
//            Response response = apiService.loadPopularArticles(7).execute();
            Response response = apiService.getPopularMovieList(ApplicationNetwork.UrlsParams.API_KEY, ApplicationNetwork.UrlsParams.LANGUAGE, 1).execute();
            assertEquals(response.code(), 200);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
