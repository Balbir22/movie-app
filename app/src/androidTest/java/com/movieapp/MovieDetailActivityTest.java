package com.movieapp;

import android.support.test.rule.ActivityTestRule;
import android.widget.ImageView;
import android.widget.TextView;

import com.movieapp.activities.MovieDetailActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MovieDetailActivityTest {

    @Rule
    public ActivityTestRule<MovieDetailActivity> movieDetailActivityTestRule = new ActivityTestRule<MovieDetailActivity>(MovieDetailActivity.class);

    private MovieDetailActivity activityMovieDetail;

    @Before
    public void setUp() throws Exception {
        activityMovieDetail = movieDetailActivityTestRule.getActivity();
    }

    @Test
    public void testMovieDetailActivityLaunch(){
        TextView screenTitleTxt = activityMovieDetail.findViewById(R.id.screenTitleTxt);
        assertNotNull("screenTitleTxt is null", screenTitleTxt);

        ImageView posterImg = activityMovieDetail.findViewById(R.id.posterImg);
        assertNotNull("posterImg is null", posterImg);

        TextView movieTitileTxt = activityMovieDetail.findViewById(R.id.movieTitleTxt);
        assertNotNull("movieTitileTxt is null", movieTitileTxt);

        TextView runningTxt = activityMovieDetail.findViewById(R.id.runningTxt);
        assertNotNull("runningTxt is null", runningTxt);

        TextView runningTimeTxt = activityMovieDetail.findViewById(R.id.runningTimeTxt);
        assertNotNull("runningTimeTxt is null", runningTimeTxt);

        TextView releaseDateTxt = activityMovieDetail.findViewById(R.id.releaseDateTxt);
        assertNotNull("releaseDateTxt is null", releaseDateTxt);

        TextView releaseTxt = activityMovieDetail.findViewById(R.id.releaseTxt);
        assertNotNull("releaseTxt is null", releaseTxt);

        TextView generesTxt = activityMovieDetail.findViewById(R.id.generesTxt);
        assertNotNull("generesTxt is null", generesTxt);

        TextView generesNameTxt = activityMovieDetail.findViewById(R.id.generesNameTxt);
        assertNotNull("generesNameTxt is null", generesNameTxt);

        TextView ratingsTxt = activityMovieDetail.findViewById(R.id.ratingsTxt);
        assertNotNull("ratingsTxt is null", ratingsTxt);

        TextView ratingsNameTxt = activityMovieDetail.findViewById(R.id.ratingsNameTxt);
        assertNotNull("ratingsNameTxt is null", ratingsNameTxt);

        TextView actorsTxt = activityMovieDetail.findViewById(R.id.actorsTxt);
        assertNotNull("actorsTxt is null", actorsTxt);

        TextView actorsNameTxt = activityMovieDetail.findViewById(R.id.actorsNameTxt);
        assertNotNull("actorsNameTxt is null", actorsNameTxt);

        TextView synopsysNameTxt = activityMovieDetail.findViewById(R.id.synopsysNameTxt);
        assertNotNull("synopsysNameTxt is null", synopsysNameTxt);
    }

    @After
    public void tearDown() throws Exception {
        activityMovieDetail = null;
    }
}