package com.movieapp;

import android.support.design.widget.TabLayout;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.movieapp.activities.MovieListActivity;
import com.movieapp.adapters.MovieAdapter;
import com.movieapp.adapters.viewholder.MovieVH;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;


@RunWith(AndroidJUnit4.class)
public class MovieListActivityTest {
    @Rule
    public ActivityTestRule<MovieListActivity> movieListActivityTestRule = new ActivityTestRule<MovieListActivity>(MovieListActivity.class);

    private MovieListActivity activityMovieList;

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
    @Before
    public void setUp() throws Exception {
        activityMovieList = movieListActivityTestRule.getActivity();
    }

    @Test
    public void testFragmentLanuch() {
        ViewPager viewPager = activityMovieList.findViewById(R.id.viewPager);
        assertNotNull("viewPager is null", viewPager);

        TabLayout tabLayout = activityMovieList.findViewById(R.id.tabLayout);
        assertNotNull("tabLayout is null", tabLayout);

        View fragmentView = viewPager.getChildAt(viewPager.getCurrentItem());
        assertNotNull("fragmentView is null", fragmentView);

        RecyclerView recyclerView = fragmentView.findViewById(R.id.popularMoviesRecycler);
        assertNotNull("recyclerView is null", recyclerView);

        MovieAdapter movieAdapter = (MovieAdapter) recyclerView.getAdapter();
        assertNotNull("movieAdapter is null", movieAdapter);

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        MovieVH movieVH = (MovieVH) recyclerView.findViewHolderForAdapterPosition(0);
        assertNotNull("Movie View Holder is null", movieVH);

        View itemView = movieVH.itemView;
        assertNotNull("Recylerview row is null", itemView);

        onView(withIndex(withId(R.id.topConstraintLayout), 0)).perform(click());
    }

    @After
    public void tearDown() throws Exception {
        activityMovieList = null;
    }

}